﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using próba2.Classes;

namespace próba2
{
    public partial class AddPatient : Form
    {

        public Patient newPatient;
        public MainForm frm1;
        public bool cancel = false;
        private bool userClose = false;
        public bool edit = false;
        public AddPatient(Patient patient=null)
        {
            InitializeComponent();
            if (patient != null)
            {
                edit = true;
                this.newPatient = patient;
                this.AddNewPatient.Text = "Zapisz zmiany";
                this.PatientPesel.Enabled = false;
                this.radioFem.Enabled = false;
                this.radioMan.Enabled = false;
                if (patient.Sex == "F")
                    this.radioFem.Checked = true;
                else
                    this.radioMan.Checked = true;
            }
            
        }
        public string textBoxName
        {
            set { PatientName.Text = value; }
        }
        public string textBoxSurname
        {
            set { PatientSurname.Text = value; }
        }
        public string textBoxPesel
        {
            set { PatientPesel.Text = value; }
        }
        private void AddPatient_Load(object sender, System.EventArgs e)
        {
            this.PatientName = null;
        }
private int CheckPesel(string pesel)
{
    int poprawna = 1;
    int s = (int)Char.GetNumericValue(pesel.ElementAt(9))%2; // cyfra związana z płcią pacjenta
    int sex;
    if (!edit)
    {
        for (int i = 0; i < MainForm.myList.Patients.Count; i++)
            if (MainForm.myList.Patients[i].Pesel == pesel)
                MessageBox.Show("Podany numer PESEL już istnieje!");
    }
    if (pesel.Length == 11)
    {
        var x = (int)Char.GetNumericValue(pesel.ElementAt(2)); //trzecia cyfra peselu(miesiac)
        var y = (int)Char.GetNumericValue(pesel.ElementAt(3)); //czwarta cyfra(miesiac)
        int xy = 10 * x + y;
        if (!(xy >= 0 && xy < 13 || xy > 20 && xy < 33))
            poprawna = -1;
        var i = (int)Char.GetNumericValue(pesel.ElementAt(4)); //piąta - odpowiedni dzień
        var j = (int)Char.GetNumericValue(pesel.ElementAt(5)); //szósta - odpowiedni dzień
        int ij = 10 * i + j;
        if (!(ij > 0 && ij < 32))
            poprawna = -2;

        if (this.radioFem.Checked)
            sex = 0;
        else
            sex = 1;
        if (sex != s)
            MessageBox.Show("Numer PESEL lub płeć jest nieprawidłowa!");
        var kontrolna = ((int)Char.GetNumericValue(pesel.ElementAt(0)) * 1 //cyfra kontrolna
            + (int)Char.GetNumericValue(pesel.ElementAt(1)) * 3
            + (int)Char.GetNumericValue(pesel.ElementAt(2)) * 7
            + (int)Char.GetNumericValue(pesel.ElementAt(3)) * 9
            + (int)Char.GetNumericValue(pesel.ElementAt(4)) * 1
            + (int)Char.GetNumericValue(pesel.ElementAt(5)) * 3
            + (int)Char.GetNumericValue(pesel.ElementAt(6)) * 7
            + (int)Char.GetNumericValue(pesel.ElementAt(7)) * 9
            + (int)Char.GetNumericValue(pesel.ElementAt(8)) * 1
            + (int)Char.GetNumericValue(pesel.ElementAt(9)) * 3
            + (int)Char.GetNumericValue(pesel.ElementAt(10))* 7) % 10;
        if (kontrolna == 0)
            poprawna = -3;
    }
    else
        poprawna = -11;
    return poprawna;
}
        public Patient CreateNewPatient()
        {
            if (this.edit == false)
            {
                this.newPatient = new Patient(PatientName.Text, PatientSurname.Text, PatientPesel.Text, (this.radioFem.Checked ? "K" : "M"));
            }
            else
            {
                this.newPatient.Name = PatientName.Text;
                this.newPatient.Surname = PatientSurname.Text;
                this.newPatient.Pesel = PatientPesel.Text;
                this.newPatient.Sex = this.radioFem.Checked ? "F" : "M";
            }
            return newPatient;
        }

        private void PatientName_TextChanged(object sender, EventArgs e)
        { 

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void AddNewPatient_Click(object sender, EventArgs e)
        {
            AddNewPatientFunc();
        }
        private void AddNewPatientFunc()
        {
            if (this.PatientName.Text != null && this.PatientSurname.Text != null && (this.radioFem.Checked || this.radioMan.Checked))
            {
                int correct = this.CheckPesel(this.PatientPesel.Text);
                switch (correct)
                {
                    case 1:
                        {
                            this.CreateNewPatient();
                            userClose = true;
                            this.Close();
                            break;
                        }
                    case -11:
                        MessageBox.Show("Pesel musi mieć 11 cyfr!");
                        break;
                    default:
                        MessageBox.Show("Numer Pesel nie jest poprawny!");
                        break;
                }
            }
            else
                MessageBox.Show("Proszę wypełnić wszytskie dostępne pola!");
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            cancel = true;
            this.Close();
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
                return;
            if (this.DialogResult == DialogResult.Cancel)
            {
                // Assume that X has been clicked and act accordingly.
                // Confirm user wants to close
                if (!userClose)
                {

                    switch (MessageBox.Show(this, "Are you sure?", "Do you want to close the window?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        //Stay on this form
                        /*case DialogResult.No:
                            e.Cancel = true;
                            break;*/
                        case DialogResult.Yes:
                            this.cancel = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
