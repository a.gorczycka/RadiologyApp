﻿namespace próba2
{
    partial class AddPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel = new System.Windows.Forms.Button();
            this.AddNewPatient = new System.Windows.Forms.Button();
            this.PatientName = new System.Windows.Forms.TextBox();
            this.AddName = new System.Windows.Forms.Label();
            this.Surname = new System.Windows.Forms.Label();
            this.Pesel = new System.Windows.Forms.Label();
            this.PatientSurname = new System.Windows.Forms.TextBox();
            this.PatientPesel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioFem = new System.Windows.Forms.RadioButton();
            this.radioMan = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(206, 220);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 0;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // AddNewPatient
            // 
            this.AddNewPatient.Location = new System.Drawing.Point(306, 220);
            this.AddNewPatient.Name = "AddNewPatient";
            this.AddNewPatient.Size = new System.Drawing.Size(122, 23);
            this.AddNewPatient.TabIndex = 1;
            this.AddNewPatient.Text = "Add patient";
            this.AddNewPatient.UseVisualStyleBackColor = true;
            this.AddNewPatient.Click += new System.EventHandler(this.AddNewPatient_Click);
            // 
            // PatientName
            // 
            this.PatientName.Location = new System.Drawing.Point(101, 23);
            this.PatientName.Name = "PatientName";
            this.PatientName.Size = new System.Drawing.Size(100, 20);
            this.PatientName.TabIndex = 2;
            this.PatientName.TextChanged += new System.EventHandler(this.PatientName_TextChanged);
            // 
            // AddName
            // 
            this.AddName.AutoSize = true;
            this.AddName.Location = new System.Drawing.Point(32, 30);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(35, 13);
            this.AddName.TabIndex = 3;
            this.AddName.Text = "Name";
            // 
            // Surname
            // 
            this.Surname.AutoSize = true;
            this.Surname.Location = new System.Drawing.Point(32, 67);
            this.Surname.Name = "Surname";
            this.Surname.Size = new System.Drawing.Size(49, 13);
            this.Surname.TabIndex = 4;
            this.Surname.Text = "Surname";
            // 
            // Pesel
            // 
            this.Pesel.AutoSize = true;
            this.Pesel.Location = new System.Drawing.Point(32, 104);
            this.Pesel.Name = "Pesel";
            this.Pesel.Size = new System.Drawing.Size(41, 13);
            this.Pesel.TabIndex = 5;
            this.Pesel.Text = "PESEL";
            // 
            // PatientSurname
            // 
            this.PatientSurname.Location = new System.Drawing.Point(101, 64);
            this.PatientSurname.Name = "PatientSurname";
            this.PatientSurname.Size = new System.Drawing.Size(100, 20);
            this.PatientSurname.TabIndex = 6;
            // 
            // PatientPesel
            // 
            this.PatientPesel.Location = new System.Drawing.Point(101, 101);
            this.PatientPesel.Name = "PatientPesel";
            this.PatientPesel.Size = new System.Drawing.Size(100, 20);
            this.PatientPesel.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Sex";
            // 
            // radioFem
            // 
            this.radioFem.AutoSize = true;
            this.radioFem.Location = new System.Drawing.Point(101, 143);
            this.radioFem.Name = "radioFem";
            this.radioFem.Size = new System.Drawing.Size(59, 17);
            this.radioFem.TabIndex = 9;
            this.radioFem.TabStop = true;
            this.radioFem.Text = "Female";
            this.radioFem.UseVisualStyleBackColor = true;
            // 
            // radioMan
            // 
            this.radioMan.AutoSize = true;
            this.radioMan.Location = new System.Drawing.Point(101, 167);
            this.radioMan.Name = "radioMan";
            this.radioMan.Size = new System.Drawing.Size(48, 17);
            this.radioMan.TabIndex = 10;
            this.radioMan.TabStop = true;
            this.radioMan.Text = "Male";
            this.radioMan.UseVisualStyleBackColor = true;
            // 
            // AddPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 265);
            this.Controls.Add(this.radioMan);
            this.Controls.Add(this.radioFem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PatientPesel);
            this.Controls.Add(this.PatientSurname);
            this.Controls.Add(this.Pesel);
            this.Controls.Add(this.Surname);
            this.Controls.Add(this.AddName);
            this.Controls.Add(this.PatientName);
            this.Controls.Add(this.AddNewPatient);
            this.Controls.Add(this.Cancel);
            this.Name = "AddPatient";
            this.Text = "Add patient";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button AddNewPatient;
        private System.Windows.Forms.TextBox PatientName;
        private System.Windows.Forms.Label AddName;
        private System.Windows.Forms.Label Surname;
        private System.Windows.Forms.Label Pesel;
        private System.Windows.Forms.TextBox PatientSurname;
        private System.Windows.Forms.TextBox PatientPesel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioFem;
        private System.Windows.Forms.RadioButton radioMan;
    }
}