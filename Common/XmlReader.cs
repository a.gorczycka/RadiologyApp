﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using próba2.Classes;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace próba2.Common
{
    static class XmlReader //do otwierania pliku xmlowego
    {
        static public PatientsList ReadXml()
        {
            PatientsList patients = null;
            
            try
            {
                string xmlPath = @"C:\Users\Anna\Documents\XMLFile1.xml";
                XmlSerializer serializer = new XmlSerializer(typeof(PatientsList));
                
                using (StreamReader sr = new StreamReader(xmlPath))
                {
                    patients = (PatientsList)serializer.Deserialize(sr);
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetBaseException());
                throw new Exception(ex.ToString());
            }
            return patients;
            
        }
        
    static public void WriteToXML(Patient pat, int choise, Picture pic, int select)
    {
        string xmlPath = @"C:\Users\Anna\Documents\XMLFile1.xml";
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(xmlPath);
        XmlNode node;
        node = xmlDoc.DocumentElement;
        switch (choise)
        {
            case 1: //add patient
                {
                    XmlElement patient = xmlDoc.CreateElement("Patient");
                    XmlElement name = xmlDoc.CreateElement("Name");
                    name.InnerText=pat.Name;
                    XmlElement surname = xmlDoc.CreateElement("Surname");
                    surname.InnerText=pat.Surname;
                    XmlElement pesel = xmlDoc.CreateElement("Pesel");
                    pesel.InnerText=pat.Pesel;
                    XmlElement sex = xmlDoc.CreateElement("Sex");
                    sex.InnerText=pat.Sex;
                    patient.AppendChild(name);
                    patient.AppendChild(surname);
                    patient.AppendChild(pesel);
                    patient.AppendChild(sex);
                    node.AppendChild(patient);
                    break;
                }
            case 2: //edit patient
                {
                    foreach (XmlNode node1 in node.ChildNodes)
                    {
                        if (node1.ChildNodes[2].InnerText == pat.Pesel)
                        {
                            if (pic == null) //change only patient's data
                            {
                                node1.ChildNodes[0].InnerText = pat.Name;
                                node1.ChildNodes[1].InnerText = pat.Surname;
                                node1.ChildNodes[3].InnerText = pat.Sex;
                            }
                            else //Editing images
                            {
                                switch (select)
                                {
                                    case 1: //dodanie zdjęcia
                                        {
                                            XmlElement picture = xmlDoc.CreateElement("Picture");
                                            XmlElement note = xmlDoc.CreateElement("Note");
                                            note.InnerText = pic.Note;
                                            XmlElement path = xmlDoc.CreateElement("Path");
                                            path.InnerText = pic.Path;
                                            XmlElement descript = xmlDoc.CreateElement("Description");
                                            descript.InnerText = pic.Description;
                                            XmlElement date = xmlDoc.CreateElement("Date");
                                            date.InnerText = pic.Date;
                                            picture.AppendChild(note);
                                            picture.AppendChild(path);
                                            picture.AppendChild(descript);
                                            picture.AppendChild(date);
                                            if (node1.ChildNodes.Count == 4)
                                            {
                                                XmlElement pictures = xmlDoc.CreateElement("Pictures");
                                                pictures.AppendChild(picture);
                                                node1.AppendChild(pictures);
                                                //node1.ChildNodes[4].AppendChild(picture);

                                            }
                                            else
                                                node1.ChildNodes[4].AppendChild(picture);
                                            string text = node1.ChildNodes[3].InnerText;
                                            break;
                                        }
                                    case 2: //usuniecie zdjecia
                                        {
                                            foreach (XmlNode node2 in node1.ChildNodes[4])
                                            {
                                                if (node2.ChildNodes[0].InnerText == pic.Note)
                                                {
                                                    node1.ChildNodes[4].RemoveChild(node2);
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    case 3:
                                        {
                                            foreach (XmlNode node2 in node1.ChildNodes[4])
                                            {
                                                if (node2.ChildNodes[0].InnerText == pic.Note)
                                                {
                                                    node2.ChildNodes[3].InnerText = pic.Description;
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                    }
                    break;
                }
            case 3: //remove patient
                {
                    foreach (XmlNode node1 in node.ChildNodes)
                    {
                        if (node1.ChildNodes[2].InnerText == pat.Pesel)
                        {
                            node.RemoveChild(node1);
                        }
                    }
                    break;
                }

        }
        xmlDoc.Save(xmlPath);
    }
    }
}
