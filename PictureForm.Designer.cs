﻿namespace próba2
{
    partial class PictureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.PatPesel = new System.Windows.Forms.Label();
            this.PatSurname = new System.Windows.Forms.Label();
            this.PatName = new System.Windows.Forms.Label();
            this.RemovePict = new System.Windows.Forms.Button();
            this.AddPicture = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SaveDsr = new System.Windows.Forms.Button();
            this.ImageDescript = new System.Windows.Forms.TextBox();
            this.PatientInfo = new System.Windows.Forms.Label();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PatSex = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SharpenImage = new System.Windows.Forms.Button();
            this.Negativ = new System.Windows.Forms.Button();
            this.Edge = new System.Windows.Forms.Button();
            this.ShowHist = new System.Windows.Forms.Button();
            this.ShowOrg = new System.Windows.Forms.Button();
            this.BlurFilter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|BMP Files (*.bmp)|*.bmp|All file" +
    "s (*.*)|*.*";
            this.openFileDialog1.Title = "Select an Image";
            // 
            // PatPesel
            // 
            this.PatPesel.AutoSize = true;
            this.PatPesel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PatPesel.Location = new System.Drawing.Point(83, 67);
            this.PatPesel.Name = "PatPesel";
            this.PatPesel.Size = new System.Drawing.Size(38, 13);
            this.PatPesel.TabIndex = 11;
            this.PatPesel.Text = "Pesel";
            // 
            // PatSurname
            // 
            this.PatSurname.AutoSize = true;
            this.PatSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PatSurname.Location = new System.Drawing.Point(82, 45);
            this.PatSurname.Name = "PatSurname";
            this.PatSurname.Size = new System.Drawing.Size(61, 13);
            this.PatSurname.TabIndex = 10;
            this.PatSurname.Text = "Nazwisko";
            // 
            // PatName
            // 
            this.PatName.AutoSize = true;
            this.PatName.Location = new System.Drawing.Point(82, 23);
            this.PatName.Name = "PatName";
            this.PatName.Size = new System.Drawing.Size(30, 13);
            this.PatName.TabIndex = 9;
            this.PatName.Text = "Imie";
            // 
            // RemovePict
            // 
            this.RemovePict.Location = new System.Drawing.Point(9, 192);
            this.RemovePict.Name = "RemovePict";
            this.RemovePict.Size = new System.Drawing.Size(105, 23);
            this.RemovePict.TabIndex = 8;
            this.RemovePict.Text = "Remove image";
            this.RemovePict.UseVisualStyleBackColor = true;
            this.RemovePict.Click += new System.EventHandler(this.RemovePict_Click);
            // 
            // AddPicture
            // 
            this.AddPicture.Location = new System.Drawing.Point(10, 163);
            this.AddPicture.Name = "AddPicture";
            this.AddPicture.Size = new System.Drawing.Size(104, 23);
            this.AddPicture.TabIndex = 7;
            this.AddPicture.Text = "Add image";
            this.AddPicture.UseVisualStyleBackColor = true;
            this.AddPicture.Click += new System.EventHandler(this.AddPicture_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(9, 107);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // SaveDsr
            // 
            this.SaveDsr.Location = new System.Drawing.Point(60, 608);
            this.SaveDsr.Name = "SaveDsr";
            this.SaveDsr.Size = new System.Drawing.Size(130, 42);
            this.SaveDsr.TabIndex = 1;
            this.SaveDsr.Text = "Save image description";
            this.SaveDsr.UseVisualStyleBackColor = true;
            this.SaveDsr.Click += new System.EventHandler(this.button1_Click);
            // 
            // ImageDescript
            // 
            this.ImageDescript.Location = new System.Drawing.Point(196, 608);
            this.ImageDescript.Multiline = true;
            this.ImageDescript.Name = "ImageDescript";
            this.ImageDescript.Size = new System.Drawing.Size(800, 117);
            this.ImageDescript.TabIndex = 4;
            // 
            // PatientInfo
            // 
            this.PatientInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PatientInfo.AutoSize = true;
            this.PatientInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PatientInfo.Location = new System.Drawing.Point(7, 46);
            this.PatientInfo.Name = "PatientInfo";
            this.PatientInfo.Size = new System.Drawing.Size(49, 13);
            this.PatientInfo.TabIndex = 2;
            this.PatientInfo.Text = "Zdjęcie";
            // 
            // imageBox2
            // 
            this.imageBox2.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.PanAndZoom;
            this.imageBox2.Location = new System.Drawing.Point(196, 2);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(800, 600);
            this.imageBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox2.TabIndex = 2;
            this.imageBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PatSex);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.PatName);
            this.groupBox1.Controls.Add(this.PatSurname);
            this.groupBox1.Controls.Add(this.PatPesel);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(187, 118);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patient Data";
            // 
            // PatSex
            // 
            this.PatSex.AutoSize = true;
            this.PatSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PatSex.Location = new System.Drawing.Point(85, 89);
            this.PatSex.Name = "PatSex";
            this.PatSex.Size = new System.Drawing.Size(34, 13);
            this.PatSex.TabIndex = 16;
            this.PatSex.Text = "Płeć";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(13, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Sex";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(13, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Pesel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(13, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(13, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.PatientInfo);
            this.groupBox2.Controls.Add(this.AddPicture);
            this.groupBox2.Controls.Add(this.RemovePict);
            this.groupBox2.Location = new System.Drawing.Point(3, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(187, 230);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Images administration";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Available images:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Current image:";
            // 
            // SharpenImage
            // 
            this.SharpenImage.Location = new System.Drawing.Point(15, 436);
            this.SharpenImage.Name = "SharpenImage";
            this.SharpenImage.Size = new System.Drawing.Size(75, 23);
            this.SharpenImage.TabIndex = 18;
            this.SharpenImage.Text = "Sharp";
            this.SharpenImage.UseVisualStyleBackColor = true;
            this.SharpenImage.Click += new System.EventHandler(this.SharpenImage_Click);
            // 
            // Negativ
            // 
            this.Negativ.Location = new System.Drawing.Point(16, 466);
            this.Negativ.Name = "Negativ";
            this.Negativ.Size = new System.Drawing.Size(75, 23);
            this.Negativ.TabIndex = 19;
            this.Negativ.Text = "Negative";
            this.Negativ.UseVisualStyleBackColor = true;
            this.Negativ.Click += new System.EventHandler(this.Negativ_Click);
            // 
            // Edge
            // 
            this.Edge.Location = new System.Drawing.Point(15, 496);
            this.Edge.Name = "Edge";
            this.Edge.Size = new System.Drawing.Size(75, 23);
            this.Edge.TabIndex = 20;
            this.Edge.Text = "Edging";
            this.Edge.UseVisualStyleBackColor = true;
            this.Edge.Click += new System.EventHandler(this.Edge_Click);
            // 
            // ShowHist
            // 
            this.ShowHist.Location = new System.Drawing.Point(15, 378);
            this.ShowHist.Name = "ShowHist";
            this.ShowHist.Size = new System.Drawing.Size(75, 23);
            this.ShowHist.TabIndex = 21;
            this.ShowHist.Text = "ShowHist";
            this.ShowHist.UseVisualStyleBackColor = true;
            this.ShowHist.Click += new System.EventHandler(this.ShowHist_Click);
            // 
            // ShowOrg
            // 
            this.ShowOrg.Location = new System.Drawing.Point(16, 555);
            this.ShowOrg.Name = "ShowOrg";
            this.ShowOrg.Size = new System.Drawing.Size(88, 23);
            this.ShowOrg.TabIndex = 22;
            this.ShowOrg.Text = "Show oryginal";
            this.ShowOrg.UseVisualStyleBackColor = true;
            this.ShowOrg.Click += new System.EventHandler(this.ShowOrg_Click);
            // 
            // BlurFilter
            // 
            this.BlurFilter.Location = new System.Drawing.Point(16, 407);
            this.BlurFilter.Name = "BlurFilter";
            this.BlurFilter.Size = new System.Drawing.Size(75, 23);
            this.BlurFilter.TabIndex = 23;
            this.BlurFilter.Text = "Blur";
            this.BlurFilter.UseVisualStyleBackColor = true;
            this.BlurFilter.Click += new System.EventHandler(this.BlurFilter_Click);
            // 
            // PictureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.BlurFilter);
            this.Controls.Add(this.ShowOrg);
            this.Controls.Add(this.ShowHist);
            this.Controls.Add(this.Edge);
            this.Controls.Add(this.Negativ);
            this.Controls.Add(this.SharpenImage);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SaveDsr);
            this.Controls.Add(this.imageBox2);
            this.Controls.Add(this.ImageDescript);
            this.Name = "PictureForm";
            this.Text = "X-ray image";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label PatPesel;
        private System.Windows.Forms.Label PatSurname;
        private System.Windows.Forms.Label PatName;
        private System.Windows.Forms.Button RemovePict;
        private System.Windows.Forms.Button AddPicture;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button SaveDsr;
        private System.Windows.Forms.TextBox ImageDescript;
        private System.Windows.Forms.Label PatientInfo;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label PatSex;
        private System.Windows.Forms.Button SharpenImage;
        private System.Windows.Forms.Button Negativ;
        private System.Windows.Forms.Button Edge;
        private System.Windows.Forms.Button ShowHist;
        private System.Windows.Forms.Button ShowOrg;
        private System.Windows.Forms.Button BlurFilter;

    }
}