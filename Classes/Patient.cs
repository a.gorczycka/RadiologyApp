﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace próba2.Classes
{
[Serializable()]
public class Patient : IComparable<Patient>
{

    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("Surname")]
    public string Surname { get; set; }
    [XmlElement("Pesel")]
    public string Pesel { get; set; }
    [XmlElement("Sex")]
    public string Sex { get; set; }
    [XmlElement("Pictures")]
    public PicturesList PicturesList { get; set; }

    public Patient()
    {
    }

    public Patient(string Name, string Surname, string Pesel, string Sex)
    {
        this.Name = Name;
        this.Surname = Surname;
        this.Pesel = Pesel;
        this.Sex = Sex;
    }

    public int CompareTo(Patient pat)
    {
        return this.Surname.CompareTo(pat.Surname);
    }
}
}
