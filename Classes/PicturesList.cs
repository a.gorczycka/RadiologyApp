﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace próba2.Classes
{
    [Serializable()]
    [XmlRoot("Pictures")]
    public class PicturesList
    {        
        [XmlElement("Picture")]
        public List<Picture> Pictures { get; set; }

        public PicturesList()
        {
            this.Pictures = new List<Picture>();
        }
        
    }
}
