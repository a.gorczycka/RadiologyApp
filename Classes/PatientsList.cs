﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace próba2.Classes
{

    [Serializable()]
    [XmlRoot("Patients")]
    public class PatientsList
    {
        [XmlElement("Patient")]
        public List<Patient> Patients { get; set; }
        

        public PatientsList()
        {
            this.Patients = new List<Patient>();
        }
        //hermetyzacja
        private void SortIt()
        {
           // Patients.Sort((x, y) => string.Compare(x.Surname, y.LastName));
            Patients.Sort();
            //Patients.OrderBy(o => o.Surname);
        }
        public void AddElement(Patient newPat)
        {
            Patients.Add(newPat);
            SortIt();
        }
        public void RemoveElement(Patient Patient)
        {
            Patients.Remove(Patient);
        }
        public void EditElement(Patient newPat, int idx)
        {
            Patients.RemoveAt(idx);
            Patients.Add(newPat);
            SortIt();
        }
    }
}
