﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace próba2.Classes
{
[Serializable()]
public class Picture : IComparable<Picture>
{
    [XmlElement("Note")]
    public string Note { get; set; }
    [XmlElement("Path")]
    public string Path { get; set; }
    [XmlElement("Date")]
    public string Date { get; set; }
    [XmlElement("Description")]
    public string Description { get; set; }

    public Picture()
    {
    }

    public Picture(string Note, string Path, string date, string Description)
    {
        this.Note = Note;
        this.Path = Path;
        this.Date = Date;
        this.Description = Description;
    }
    public int CompareTo(Picture pic)
    {
        return this.Note.CompareTo(pic.Note);
    }
}
}
