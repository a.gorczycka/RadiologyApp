﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using próba2.Classes;

namespace próba2
{
    public partial class AddImageForm : Form
    {
        string filePath;
        public Picture newPicture;
        public bool cancel = false;
        private bool userClose = false;
        public AddImageForm()
        {
            InitializeComponent();
            OpenFileDialog();
        }
        private void OpenFileDialog()
        {
            Stream myStream = null;
            this.openFileDialog1.Filter = "BMP Files (*.bmp)|*.bmp|Dicom files (*.dcm)|*.dcm|JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|All files (*.*)|*.*";
           // this.openFileDialog1.ShowDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        filePath = openFileDialog1.FileName;
                        //fileFolder = openFileDialog1.FileName\\
                        this.Sciezka.Text = openFileDialog1.SafeFileName;
                        //this.Close();
                        //using (myStream)
                        //{

                        //    // Insert code to read the stream here.
                        //}
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
        private Picture CreateImage()
        {
            DateTime currDate = DateTime.Now;
            this.newPicture = new Picture(PictureName.Text, filePath, currDate.ToString(), Description.Text);
            return newPicture;
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog();
        }

        private void AddImage_Click(object sender, EventArgs e)
        {
            if (this.PictureName.Text != null)
            {
                CreateImage();
                userClose = true;
                this.Close();
            }
            else
                MessageBox.Show("Należy wpisac nazwę zdjecia.");
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            cancel = true;
            this.Close();

        }

        private void Sciezka_Click(object sender, EventArgs e)
        {

        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
                return;
            if (this.DialogResult == DialogResult.Cancel)
            {
                if (!userClose)
                {

                    switch (MessageBox.Show(this, "Are you sure?", "Do you want to close the window?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        //Stay on this form
                        /*case DialogResult.No:
                            e.Cancel = true;
                            break;*/
                        case DialogResult.Yes:
                            this.cancel = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }


    }
}