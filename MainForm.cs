﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using próba2.Classes;
using System.Xml;



namespace próba2
{
    public partial class MainForm : Form
    {

        public static PatientsList myList;
        BindingSource source;
                
        public MainForm()
        {
            InitializeComponent();

        try
        {
            myList = Common.XmlReader.ReadXml();
            // sorted = myList.Patients.OrderBy(o => o.Surname).ToList();
            myList.Patients.Sort();
            dataGridView1.DataSource = myList.Patients;
            dataGridView1.Columns[4].Visible = false;          
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.ToString());
            return;
        }
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
        }
            
        private void button1_Click(object sender, EventArgs e) //przycisk dodaj
        {
            AddPatientFunc();           
        }
        private void Edit_Click(object sender, EventArgs e)
        {
            EditPatientFunc(this.dataGridView1.CurrentCell.RowIndex);
        } //przcisk edycji
        private void EditPatientFunc(int row)////funkcja edycji
        {
            using (AddPatient addForm = new AddPatient(myList.Patients[row]))
            {
                addForm.textBoxName = myList.Patients[row].Name;
                addForm.textBoxSurname = myList.Patients[row].Surname;
                addForm.textBoxPesel = myList.Patients[row].Pesel;
                addForm.ShowDialog();
                if (!addForm.cancel)
                {

                    Patient newPatient = addForm.newPatient;
                    myList.EditElement(newPatient, row);
                    //myList.Patients.Remove(sorted[row]);
                    //myList.Patients.Add(newPatient);
                    Common.XmlReader.WriteToXML(myList.Patients[row], 2, null, 0);
                    RefreshData();
                }
            }
        }   
        private void AddPatientFunc()////funkcja dodawania
        {
            using (AddPatient addForm = new AddPatient())
            {
                addForm.ShowDialog();
                if (!addForm.cancel) //klikniecie anuluj
                {
                    Patient newPatient = addForm.newPatient;
                    //myList.Patients.Add(newPatient);
                    myList.AddElement(newPatient);
                    Common.XmlReader.WriteToXML(newPatient, 1, null, 0);
                    RefreshData();
                }
            }
            
        }   
        private void DeletePatient_Click(object sender, EventArgs e)//przycisk usuwania
        {
            DialogResult dr = MessageBox.Show("Do you want remove the patient?",
                      "Delete Patient.", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    RemovePatient();
                    break;
                case DialogResult.No:
                    break;
            }
            
            //RefreshData();
        }
        private void RemovePatient()
        {
            Patient patToRemove = myList.Patients[dataGridView1.CurrentCell.RowIndex];
            myList.RemoveElement(patToRemove);
            //myList.Patients.Remove(sorted[dataGridView1.CurrentCell.RowIndex]);
            Common.XmlReader.WriteToXML(patToRemove, 3, null, 0);
            RefreshData();
            MessageBox.Show("Patient was removed from database");
        }
        private void button2_Click(object sender, EventArgs e)  //wyszukaj pacjenta
        {
            SearchPatient();
        }
        private void SearchPatient()
        {
            bool found = false;
            for (int i = 0; i < myList.Patients.Count; i++)
            {

                if ((this.BySurname.Checked ? myList.Patients[i].Surname : myList.Patients[i].Pesel) == this.SearchBox.Text)
                {
                    dataGridView1.CurrentCell = dataGridView1.Rows[i].Cells[0];
                    found = true;
                    break;
                }                    
            }
            if(!found)
                MessageBox.Show("Patient was not found.");
        }

        private void ShowPicture_Click(object sender, EventArgs e) //jest enable gdy wybrano jakies zdjecie
        {
            int row = dataGridView1.CurrentCell.RowIndex;
            using (PictureForm pictureForm = new PictureForm(myList.Patients[row], false))
            {
                pictureForm.ShowDialog();
            }
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            //int row = dataGridView1.CurrentCell.RowIndex;
            //if (myList.Patients[row].PicturesList != null)
            //{
            //    this.ShowPicture.Enabled = true;
            //}
            //else
            //    this.ShowPicture.Enabled = false;
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }

        
        public void RefreshData()
        {
            source = new BindingSource();
            source.DataSource = myList.Patients;
            dataGridView1.DataSource = source;
        }

        public bool dat { get; set; }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            switch (MessageBox.Show(this, "Are you sure?", "Do you want to quit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                //Stay on this form
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                //case DialogResult.Yes:
                //    this.Close();
                //    break;
                default:
                    break;
            }
        }
        
    }
}
