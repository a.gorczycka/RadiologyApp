﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using RasterEdge.Imaging.Basic;
using RasterEdge.Imaging.DICOM;
//using RasterEdge.XImage.WinFormsViewer.Enum;
//using RasterEdge.XImage.WinFormsViewer;
using EvilDicom.Components;
using openDicom.Image;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.Util;
using Emgu.CV.UI;
using próba2.Classes;


namespace próba2
{
    public partial class PictureForm : Form
    {
        public Picture Picture { get; set; }
        public Patient Patient { get; set; }

        Image<Bgr, byte> img, img2;
        DCMDocument dcmDoc;
        string imgName, filePath, folder, fileExt;
        //bool showOrgImg = false;
        public PictureForm(Patient patient, bool showOrgImg)
        {
            InitializeComponent();
            
            this.Patient = patient;
            if (Patient.PicturesList == null || Patient.PicturesList.Pictures.Count == 0)
            {
                MessageBox.Show("The patient does not have any photos, add photos of the patient");
                AddPictureFunc();
            }
            else
            {
                Patient.PicturesList.Pictures.Sort();
                this.Picture = this.Patient.PicturesList.Pictures[0];

                CheckIfExist(this.Picture);
                img = new Image<Bgr, byte>(filePath);
                img2 = new Image<Bgr, byte>(filePath);
                foreach (var image in Patient.PicturesList.Pictures)
                {
                    comboBox1.Items.Add(image);
                    comboBox1.DisplayMember = "Note";
                }
                this.imageBox2.Image = img;
                this.PatientInfo.Text = Picture.Note;
                this.ImageDescript.Text = Picture.Description;
            }

            this.PatName.Text = this.Patient.Name;
            this.PatSurname.Text = this.Patient.Surname;
            this.PatPesel.Text = this.Patient.Pesel;
            this.PatSex.Text = this.Patient.Sex;

           
        }
        public PictureForm()
        {
            InitializeComponent();
        }
        private void CheckIfExist(Picture Picture)
        {
            
            imgName = System.IO.Path.GetFileName(Picture.Path);
            folder = System.IO.Path.GetDirectoryName(Picture.Path);
            int position = imgName.LastIndexOf('.');
            fileExt = imgName.Substring(position, imgName.Length - position);
        if (fileExt == ".dcm")
        {
            imgName = imgName.Substring(0, imgName.LastIndexOf("."));
            filePath = folder + "\\0" + imgName + ".bmp";
            FileInfo File = new FileInfo(filePath);
            if (!File.Exists)
            {
                dcmDoc = new DCMDocument(Picture.Path);
                dcmDoc.ConvertToImages(ImageType.BMP, folder, imgName);
            }
        }
        else
            filePath = Picture.Path;
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int row = comboBox1.SelectedIndex;
            this.Picture = this.Patient.PicturesList.Pictures[row];
            CheckIfExist(this.Picture);

            RefreshData(true);

        }
        private void Negativ_Click(object sender, EventArgs e)
        {
            //Image<Bgr, Byte> img2 = img;
            Negative(img2);
            this.imageBox2.Image = img2;
        }
        private Image<Bgr, Byte> Negative(Image<Bgr, Byte> image)
        {
            int red, green, blue, set_red, set_green, set_blue;
            int height = image.Height;
            int width = image.Width;
            for (int x = 0; x < height; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    blue = image.Data[x, y, 0];
                    green = image.Data[x, y, 1];
                    red = image.Data[x, y, 2];

                    set_blue = 256 - blue;
                    set_green = 256 - green;
                    set_red = 256 - red;

                    image.Data[x, y, 0] = (byte)set_blue;
                    image.Data[x, y, 1] = (byte)set_green;
                    image.Data[x, y, 2] = (byte)set_red;
                }
            }
            return image;
        }

        private void BlurFilter_Click(object sender, EventArgs e)
        {
            //Image<Bgr, Byte> img2 = img;
            Blur(img2);
            this.imageBox2.Image = img2;
        }
        private Image<Bgr, Byte> Blur(Image<Bgr, Byte> image)
        {
            Image<Bgr, Byte> image2 = new Image<Bgr, byte>(filePath);
            int red, green, blue, set_red, set_green, set_blue;
            double r, g, b, sum_r, sum_g, sum_b, suma_all, val_min;
            int counter, number;
            int height = image.Height;
            int width = image.Width;
            double[] tab_r = new double[9];
		    double[] tab_g = new double[9];
            double[] tab_b = new double[9];
            for (int x = 2; x < height-2; x++)
            {
                for (int y = 2; y < width-2; y++)
                {
                    counter = 0; //ktora dyspersja
                    number = 0;   //numer sąsiedztwa o najmniejszej dyspersji
                    val_min = 256;
                    for (int row = 0; row< 9; row++)
                    {
                        tab_r[row] = 0;
                        tab_g[row] = 0;
                        tab_b[row] = 0;
                    }

                    for (int m = x - 2; m < x + 1; m++) //okreslanie polozenia siatki wzgledem piksela
                    {
                        for (int n = y - 2; n < y + 1; n++)
                        {

                            sum_r = 0;
                            sum_g = 0;
                            sum_b = 0;

                            for (int i = 0; i < 3; i++) //iterowanie w siatce
                            {
                                for (int j = 0; j < 3; j++)
                                {
                                    blue = image2.Data[m + i, n + j, 0];
                                    green = image2.Data[m + i, n + j, 1];
                                    red = image2.Data[m + i, n + j, 2];

                                    tab_r[counter] += red;
                                    tab_g[counter] += green;
                                    tab_b[counter] += blue;

                                }
                            }

                            tab_r[counter] /= 9;	//otrzymuje juz srednia
                            tab_g[counter] /= 9;
                            tab_b[counter] /= 9;

                            for (int i = 0; i < 3; i++) //iterowanie w siatce 
                            {
                                for (int j = 0; j < 3; j++)
                                {
                                    blue = image2.Data[m + i, n + j, 0];
                                    green = image2.Data[m + i, n + j, 1];
                                    red = image2.Data[m + i, n + j, 2];

                                    //odejmuje i podnosze do kwadratu
                                    sum_r += (red - tab_r[counter]) * (red - tab_r[counter]);
                                    sum_g += (green - tab_g[counter]) * (green - tab_g[counter]);
                                    sum_b += (blue - tab_b[counter]) * (blue - tab_b[counter]);


                                }
                            }

                            suma_all = sum_r + sum_g + sum_b;

                            suma_all /= 9;

                            if (val_min > suma_all)
                            {
                                val_min = suma_all;
                                number = counter;
                            }

                            counter++;
                        }
                    }
                    set_blue = (int)tab_b[number];
                    set_green = (int)tab_g[number];
                    set_red = (int)tab_r[number];
                    image.Data[x, y, 0] = (byte)set_blue;
                    image.Data[x, y, 1] = (byte)set_green;
                    image.Data[x, y, 2] = (byte)set_red;
                }
            }
            return image;
        }
        private void SharpenImage_Click(object sender, EventArgs e)
        {
           //Image<Bgr, Byte> img2 = img;
           Sharpen(img2);
           this.imageBox2.Image = img2;
        }
        private Image<Bgr, Byte> Sharpen(Image<Bgr, Byte> image)
        {
            Image<Bgr, Byte> image2 = new Image<Bgr, byte>(filePath);
            int red, green, blue, set_red, set_green, set_blue;
            int min_R, min_G, min_B, max_R, max_G, max_B;
            int n = 5; // parametr
            double W_R, W_G, W_B; 
            int height = image.Height;
            int width = image.Width;
            int a = n * n;
            n = n / 2;
            for (int x = n; x < height - n; x++)
            {
                for (int y = n; y < width - n; y++)
                {
                    W_R = 0;
                    W_G = 0;
                    W_B = 0;
                    set_red = 0;
                    set_green = 0;
                    set_blue = 0;
                    min_R = 256;
                    min_G = 256;
                    min_B = 256;
                    max_R = 0;
                    max_G = 0;
                    max_B = 0;
                    
                    for (int i = x - n; i <= (x + n); i++)
                    {
                        for (int j = y - n; j <= (y + n); j++)
                        {
                            //get a pixel
                            blue = (int)image2.Data[i, j, 0];
                            green = (int)image2.Data[i, j, 1];
                            red = (int)image2.Data[i, j, 2];

                            if (min_R > red) //odczytywanie minimum  z lokalnego sąsiedztwa
                                min_R = red;
                            if (max_R < red) //odczyt maximum z lok sąsiedztwa
                                max_R = red;

                            if (min_G > green)
                                min_G = green;
                            if (max_G < green)
                                max_G = green;

                            if (min_B > blue)
                                min_B = blue;
                            if (max_B < blue)
                                max_B = blue;

                            W_R += red;  //suma
                            W_G += green;
                            W_B += blue;

                        }
                    }
                    W_R /= a;  //średnia
                    W_G /= a;
                    W_B /= a;

                    blue = image.Data[x, y, 0];
                    green = image.Data[x, y, 1];
                    red = image.Data[x, y, 2];
	
                    if (red - min_R < max_R - red)
                        set_red = min_R;
                    else
                        set_red = max_R;

                    if (green - min_G < max_G - green)
                        set_green = min_G;
                    else
                        set_green = max_G;

                    if (blue - min_B < max_B - blue)
                        set_blue = min_B;
                    else
                        set_blue = max_B;
                    
                    //setting a pixel*/

                    image.Data[x, y, 0] = (byte)set_blue;
                    image.Data[x, y, 1] = (byte)set_green;
                    image.Data[x, y, 2] = (byte)set_red;

                }
            }
            return image;
        }
        private void Edge_Click(object sender, EventArgs e)
        {
            
            Edging(img2);
            this.imageBox2.Image = img2;
        }
        private Image<Bgr, Byte> Edging(Image<Bgr, Byte> image)
        {
            Image<Bgr, Byte> image2 = new Image<Bgr,byte>(filePath);
            double red, green, blue, set_red, set_green, set_blue;
            int height = image.Height;
            int width = image.Width;
            int n = 7;
            int a = n * n;
            a = a - 1;
            n = n / 2;
            for (int x = n; x < height-n; x++)
            {
                for (int y = n; y < width-n; y++)
                {
                    set_blue = 0;
                    set_green = 0;
                    set_red = 0;

                    for (int i = x - n; i <= (x + n); i++)
                    {
                        for (int j = y - n; j <= (y + n); j++)
                        {
                            //get a pixel
                            blue = image2.Data[i, j, 0];
                            green = image2.Data[i, j, 1];
                            red = image2.Data[i, j, 2];

                            if (i == x && j == y)
                            {
                                blue = blue * a;
                                green = green * a;
                                red = red * a;
                            }
                            else
                            {
                                blue = blue * (-1);
                                green = green * (-1);
                                red = red * (-1);
                            }

                            set_blue += blue;
                            set_green += green;
                            set_red += red;
                    
                        }
                    }
                    set_blue = Math.Min(255, Math.Max(set_blue, 0));
                    set_green = Math.Min(255, Math.Max(set_green, 0));
                    set_red = Math.Min(255, Math.Max(set_red, 0));

                    image.Data[x, y, 0] = (Byte)set_blue;
                    image.Data[x, y, 1] = (Byte)set_green;
                    image.Data[x, y, 2] = (Byte)set_red;
                }
            }
            return image;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Picture.Description = ImageDescript.Text;
            Common.XmlReader.WriteToXML(Patient, 2, Picture, 3);
        }

        private void ShowHist_Click(object sender, EventArgs e)
        {       
            HistogramViewer.Show(img);           
        }

        private void AddPicture_Click(object sender, EventArgs e)
        {
            AddPictureFunc();
        }
        private void AddPictureFunc()
        {
            using (AddImageForm addImgForm = new AddImageForm())
            {
                addImgForm.ShowDialog();
                if (!addImgForm.cancel)
                {
                    if (this.Patient.PicturesList == null)
                        this.Patient.PicturesList = new PicturesList();
                    Picture newImage = addImgForm.newPicture;
                    Patient.PicturesList.Pictures.Add(newImage);
                    Patient.PicturesList.Pictures.Sort();
                    this.Picture = newImage;
                    CheckIfExist(this.Picture);
                    Common.XmlReader.WriteToXML(Patient, 2, newImage, 1);
                    RefreshData(true);
                }
            }
        }

        private void RemovePict_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you want to remove the image?",
                      "Delete image.", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    RemovePicture();
                    break;
                case DialogResult.No:
                    break;
            }
                      

        }
        private void RemovePicture()
        {
            Patient.PicturesList.Pictures.Remove(Picture);
            Common.XmlReader.WriteToXML(Patient, 2, Picture, 2);
            if (Patient.PicturesList == null || Patient.PicturesList.Pictures.Count == 0)
            {
                Picture = null;
                this.imageBox2.Image = null;
                this.PatientInfo.Text = Picture.Note;
                this.ImageDescript.Text = Picture.Description;
                //MessageBox.Show("Pacjent nie ma więcej zdjęć, dodaj zdjęcia pacjenta(1) lub wróć do głównego okna");
                DialogResult dr = MessageBox.Show("The patient does not have more images. Do you want to add an image of the patient?",
                      "No more images of the patient", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        AddPictureFunc();
                        break;
                    case DialogResult.No:
                        this.Close();
                        break;
                }
                
            }
            RefreshData(false);
        }
        private void ShowOrg_Click(object sender, EventArgs e)
        {
            this.imageBox2.Image = img;
        }
        private void RefreshData(bool add)
        {
            if (add)
            {
                img = new Image<Bgr, byte>(filePath);
                img2 = new Image<Bgr, byte>(filePath);
                this.PatientInfo.Text = Picture.Note;
                this.ImageDescript.Text = Picture.Description;
                this.imageBox2.Image = img;
            }
            else
            {
                this.imageBox2.Image = null;
                this.PatientInfo.Text = null;
                this.ImageDescript.Text = null;
            }
            comboBox1.Items.Clear();
            foreach (var image in Patient.PicturesList.Pictures)
            {
                comboBox1.Items.Add(image);
                comboBox1.DisplayMember = "Note";
            }
        }

        private void FormClose(object sender, FormClosingEventArgs e)
        {
            switch (MessageBox.Show(this, "Are you sure?", "Do you want to quit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                //Stay on this form
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                //case DialogResult.Yes:
                //    this.Close();
                //    break;
                default:
                    break;
            }
        }

     

        
    }
}
