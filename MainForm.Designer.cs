﻿namespace próba2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.DeletePatient = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ByPesel = new System.Windows.Forms.RadioButton();
            this.BySurname = new System.Windows.Forms.RadioButton();
            this.ShowPicture = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(20, 41);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 36);
            this.Add.TabIndex = 1;
            this.Add.Text = "Add patient";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(125, 27);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(390, 366);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DeletePatient
            // 
            this.DeletePatient.Location = new System.Drawing.Point(20, 141);
            this.DeletePatient.Name = "DeletePatient";
            this.DeletePatient.Size = new System.Drawing.Size(75, 36);
            this.DeletePatient.TabIndex = 4;
            this.DeletePatient.Text = "Remove Patient";
            this.DeletePatient.UseVisualStyleBackColor = true;
            this.DeletePatient.Click += new System.EventHandler(this.DeletePatient_Click);
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(20, 102);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(75, 23);
            this.Edit.TabIndex = 2;
            this.Edit.Text = "Edit patient";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // SearchBox
            // 
            this.SearchBox.Location = new System.Drawing.Point(6, 58);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(100, 20);
            this.SearchBox.TabIndex = 8;
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ByPesel);
            this.groupBox1.Controls.Add(this.BySurname);
            this.groupBox1.Controls.Add(this.SearchBox);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(530, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 98);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search patient";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // ByPesel
            // 
            this.ByPesel.AutoSize = true;
            this.ByPesel.Location = new System.Drawing.Point(98, 20);
            this.ByPesel.Name = "ByPesel";
            this.ByPesel.Size = new System.Drawing.Size(51, 17);
            this.ByPesel.TabIndex = 11;
            this.ByPesel.TabStop = true;
            this.ByPesel.Text = "Pesel";
            this.ByPesel.UseVisualStyleBackColor = true;
            // 
            // BySurname
            // 
            this.BySurname.AutoSize = true;
            this.BySurname.Location = new System.Drawing.Point(7, 20);
            this.BySurname.Name = "BySurname";
            this.BySurname.Size = new System.Drawing.Size(67, 17);
            this.BySurname.TabIndex = 10;
            this.BySurname.TabStop = true;
            this.BySurname.Text = "Surname";
            this.BySurname.UseVisualStyleBackColor = true;
            // 
            // ShowPicture
            // 
            this.ShowPicture.Location = new System.Drawing.Point(530, 182);
            this.ShowPicture.Name = "ShowPicture";
            this.ShowPicture.Size = new System.Drawing.Size(99, 44);
            this.ShowPicture.TabIndex = 11;
            this.ShowPicture.Text = "View images";
            this.ShowPicture.UseVisualStyleBackColor = true;
            this.ShowPicture.Click += new System.EventHandler(this.ShowPicture_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 463);
            this.Controls.Add(this.ShowPicture);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.DeletePatient);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Add);
            this.Name = "MainForm";
            this.Text = "Program for the radiologist";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button DeletePatient;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton ByPesel;
        private System.Windows.Forms.RadioButton BySurname;
        private System.Windows.Forms.Button ShowPicture;
    }
}

