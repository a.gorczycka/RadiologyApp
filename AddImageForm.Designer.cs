﻿namespace próba2
{
    partial class AddImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Description = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.PictureName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AddImage = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.ChooseAnother = new System.Windows.Forms.Button();
            this.Sciezka = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Description
            // 
            this.Description.Location = new System.Drawing.Point(125, 108);
            this.Description.Multiline = true;
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(327, 108);
            this.Description.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Image description:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PictureName
            // 
            this.PictureName.Location = new System.Drawing.Point(125, 65);
            this.PictureName.Name = "PictureName";
            this.PictureName.Size = new System.Drawing.Size(100, 20);
            this.PictureName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Image note:";
            // 
            // AddImage
            // 
            this.AddImage.Location = new System.Drawing.Point(367, 258);
            this.AddImage.Name = "AddImage";
            this.AddImage.Size = new System.Drawing.Size(85, 23);
            this.AddImage.TabIndex = 3;
            this.AddImage.Text = "Add image";
            this.AddImage.UseVisualStyleBackColor = true;
            this.AddImage.Click += new System.EventHandler(this.AddImage_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(265, 258);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 5;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // ChooseAnother
            // 
            this.ChooseAnother.Location = new System.Drawing.Point(32, 258);
            this.ChooseAnother.Name = "ChooseAnother";
            this.ChooseAnother.Size = new System.Drawing.Size(132, 23);
            this.ChooseAnother.TabIndex = 6;
            this.ChooseAnother.Text = "Select different image";
            this.ChooseAnother.UseVisualStyleBackColor = true;
            this.ChooseAnother.Click += new System.EventHandler(this.button3_Click);
            // 
            // Sciezka
            // 
            this.Sciezka.AutoSize = true;
            this.Sciezka.Location = new System.Drawing.Point(122, 21);
            this.Sciezka.Name = "Sciezka";
            this.Sciezka.Size = new System.Drawing.Size(35, 13);
            this.Sciezka.TabIndex = 7;
            this.Sciezka.Text = "image";
            this.Sciezka.Click += new System.EventHandler(this.Sciezka_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Selected file:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "(not required)";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // AddImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 313);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Sciezka);
            this.Controls.Add(this.ChooseAnother);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.AddImage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PictureName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Description);
            this.Name = "AddImageForm";
            this.Text = "Add new image";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClose);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Description;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox PictureName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddImage;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button ChooseAnother;
        private System.Windows.Forms.Label Sciezka;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}